﻿using UnityEngine;

public class Rotator : MonoBehaviour
{
    public Vector3 vector3; // Rotation pro Sekunde



    void Update()
    {
        //transform.rotation = Quaternion.EulerAngles(new Vector3(1, 1, 1)); // Setzen eines festen Wertes
        transform.Rotate(vector3 * Time.deltaTime); // Rotation über Zeit
    }
}
