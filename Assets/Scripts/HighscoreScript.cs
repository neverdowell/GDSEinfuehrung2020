﻿using UnityEngine;
using UnityEngine.UI;

public class HighscoreScript : MonoBehaviour {

    public string GameObjectName;
    public Text textGameObject;
    public bool zeigeGanzeZahl = false;

    private string valueName = "Highscore";

    private void Start() {
        if (textGameObject == null) {
            textGameObject = GameObject.Find(GameObjectName).GetComponent<Text>();
            LoadValue();
        }
    }

    #region ui callbacks
    public void LoadValue() {
        ChangePointsScript.points = PlayerPrefs.GetFloat(valueName, 0);
        if (zeigeGanzeZahl) { 
            textGameObject.text = ((int)ChangePointsScript.points).ToString();
        } else {
            textGameObject.text = ChangePointsScript.points.ToString();
        }
    }

    public void SaveValue() {
        PlayerPrefs.SetFloat(valueName, ChangePointsScript.points);
    }

    public void ResetValue() {
        ChangePointsScript.points = 0;
        SaveValue();
    }
    #endregion
}
