﻿using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    public GameObject target;

    void Update()
    {
        transform.LookAt(target.transform);
    }
}
