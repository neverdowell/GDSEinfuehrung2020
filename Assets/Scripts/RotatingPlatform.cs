﻿using UnityEngine;

public class RotatingPlatform : MonoBehaviour{

    public KeyCode rotateClockwiseKey;
    public KeyCode rotateCounterClockwiseKey;

    public float rotationSpeedZ; // degree per second
    public float maxRotationZ; // max value 
    public float currentRotationZ;



    private float rotateBackFactor = 1;



    #region lifecycle
    void Start() {
        currentRotationZ = 0;
    }

    void Update(){
        if (Input.GetKey(rotateClockwiseKey)){
            currentRotationZ = currentRotationZ - rotationSpeedZ * Time.deltaTime;
        } else if (Input.GetKey(rotateCounterClockwiseKey)){
            currentRotationZ = currentRotationZ + rotationSpeedZ * Time.deltaTime;
        } else {
            currentRotationZ *= rotateBackFactor * Time.deltaTime;
        }
        currentRotationZ = Mathf.Clamp(currentRotationZ, -maxRotationZ, maxRotationZ);

        transform.localRotation = Quaternion.Euler(0, 0, currentRotationZ);
    }
    #endregion
}
