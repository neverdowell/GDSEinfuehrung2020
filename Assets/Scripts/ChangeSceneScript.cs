﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneScript : MonoBehaviour
{
    public string sceneName;


    #region lifecycle
    private void OnCollisionEnter( Collision collision ) {
        changeScene(sceneName);
    }

    private void OnTriggerEnter( Collider other ) {
        changeScene(sceneName);
    }
    #endregion




    #region helper
    public void changeScene(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }
    #endregion

}
