﻿using UnityEngine;
using UnityEngine.UI;

public class ChangePointsScript : MonoBehaviour{

    public string GameObjectName;
    public Text textGameObject;
    public bool zeigeGanzeZahl = false;
    public float punktVeränderung;

    public static float points;

    private void Start() {
        if (textGameObject == null) {
            textGameObject = GameObject.Find(GameObjectName).GetComponent<Text>();
        }
    }

    private void OnCollisionEnter( Collision collision ) {
        handleCollision();
    }

    private void OnTriggerEnter( Collider other ) {
        handleCollision();
    }

    #region helper
    private void handleCollision() {
        points += punktVeränderung;
        if (zeigeGanzeZahl) {
            textGameObject.text = ((int)points).ToString();
        } else {
            textGameObject.text = points.ToString();
        }
    }
    #endregion
}
