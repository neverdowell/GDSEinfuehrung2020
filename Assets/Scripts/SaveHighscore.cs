﻿using UnityEngine;

public class SaveHighscore : MonoBehaviour
{
    private string valueName = "Highscore";

    #region lifecycle
    private void OnCollisionEnter( Collision collision ) {
        SaveValue();
    }

    private void OnTriggerEnter( Collider other ) {
        SaveValue();
    }
    #endregion



    #region helper
    public void SaveValue() {
        PlayerPrefs.SetFloat(valueName, ChangePointsScript.points);
    }
    #endregion
}
