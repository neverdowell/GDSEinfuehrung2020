﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed;
    public float scaleFactor;
    public bool istKinetisch = false;


    int x = 0;
    int z = 0;
    Rigidbody rigidbody;

    private void Start() {
        rigidbody = GetComponent<Rigidbody>();
    }

    void Update() {
        if (istKinetisch) { 
            moveKinetisch();
        } else { 
            movePhysikalisch();
        }

        // Gravity an/aus 
        if (Input.GetKeyDown(KeyCode.F)) {
            GetComponent<Rigidbody>().useGravity = !(GetComponent<Rigidbody>().useGravity);
        }

        // Skalierung an/aus
        if (Input.GetKeyDown(KeyCode.Space)) {
            if (transform.localScale.x == 1) {
                transform.localScale = transform.localScale * scaleFactor;
            } else {
                transform.localScale = Vector3.one;//transform.localScale / scaleFactor;
            }
        }

        // Mesh anzeigen/verstecken
        if (Input.GetKeyDown(KeyCode.Return)) {
            GetComponent<MeshRenderer>().enabled = !GetComponent<MeshRenderer>().enabled;
        }

        // Zerstören des Players
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Destroy(this.gameObject); // Destroy kann Components, GameObjects zerstören
            //Destroy(this.gameObject, 3.0f); // Mit Zeitverzögerung von 3.0 Sekunden
        }

        // Aktivieren/Deaktivieren des Players (GameObject)
        if (Input.GetKeyDown(KeyCode.LeftShift)) {
            this.gameObject.active = !this.gameObject.active;
        }
    }

    private void movePhysikalisch() {
        if (Input.GetKey(KeyCode.W)) {
            rigidbody.AddForce(Vector3.forward * Time.deltaTime * speed, ForceMode.VelocityChange);
        }
        if (Input.GetKey(KeyCode.S)) {
            rigidbody.AddForce(Vector3.back * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.D)) {
            rigidbody.AddForce(Vector3.right * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.A)) {
            rigidbody.AddForce(Vector3.left * Time.deltaTime * speed);
        }
        
    }


    private void moveKinetisch() {
        // Bewegung (kinetisch)
        x = 0; // Bewegung in x-Richtung
        z = 0; // Bewegung in z-Richtung

        if (Input.GetKey(KeyCode.W)) {
            z = 1;
        }
        if (Input.GetKey(KeyCode.S)) {
            z = -1;
        }
        if (Input.GetKey(KeyCode.D)) {
            x = 1;
        }
        if (Input.GetKey(KeyCode.A)) {
            x = -1;
        }
        transform.Translate(new Vector3(x, 0, z) * Time.deltaTime * speed);
    }
}
