﻿using UnityEngine;

public class Mover : MonoBehaviour
{
    public Vector3 vector3; // Bewegung pro Sekunde




    void Update()
    {
        //transform.Translate(vector3 * Time.deltaTime); // Setzen eines festen Wertes
        transform.position = transform.position + vector3 * Time.deltaTime; // Bewegen über Zeit
    }
}
