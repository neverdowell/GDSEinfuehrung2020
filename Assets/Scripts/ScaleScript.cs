﻿using UnityEngine;

public class ScaleScript : MonoBehaviour
{
    public float scaleMultiplier = 1; // < 1: Objekt wird kleiner
                                      // = 1: keine Größenänderung
                                      // > 1: Objekt wird größer
    public bool setFixScale = false;  //  

    #region lifecycle
    private void OnCollisionEnter( Collision collision ) {
        handleScaling(collision.gameObject);
    }

    private void OnTriggerEnter( Collider other ) {
        handleScaling(other.gameObject);
    }
    #endregion




    #region helper
    public void handleScaling(GameObject scalingGameObject) {
        if (setFixScale) {
            scalingGameObject.transform.localScale = Vector3.one * scaleMultiplier;
        } else {
            scalingGameObject.transform.localScale *= scaleMultiplier;
        }
    }
    #endregion
}
