﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUIScript : MonoBehaviour
{
    public void OnClickStartButton(string sceneName){
        SceneManager.LoadScene(sceneName);
    }

    public void OnValueChangedSlider( float zahl ) {
        //SceneManager.LoadScene(sceneName);
        Debug.Log("Wert ist " + zahl);
    }
}
