﻿using UnityEngine;

public class ChangeColorScript : MonoBehaviour{

    public Color MouseOverColor;
    public Color oldColor;

    private void OnMouseEnter() {
        //GetComponent<MeshRenderer>().material.color = Color.blue;
        //GetComponent<MeshRenderer>().material.color = new Color(1,0,0);
        oldColor = GetComponent<MeshRenderer>().material.color;
        GetComponent<MeshRenderer>().material.color = MouseOverColor;
    }

    private void OnMouseExit() {
        GetComponent<MeshRenderer>().material.color = oldColor;
    }
}
