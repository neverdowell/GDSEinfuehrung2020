﻿using UnityEngine;

public class DestroyOnCollision : MonoBehaviour{
    public bool destroyPlayer = false;
    public bool destroyOther = false;

    private void OnCollisionEnter( Collision collision ) { // Zerstöre das ANDERE objekt
        if (destroyOther) {
            Destroy(this.gameObject);
        }
        if (destroyPlayer) {
            Destroy(collision.gameObject);
        }
    }

    private void OnTriggerEnter( Collider other ) {
        if (destroyOther) {
            Destroy(this.gameObject);
        }
        if (destroyPlayer) {
            Destroy(other.gameObject);
        }
    }
}
