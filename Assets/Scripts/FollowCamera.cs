﻿using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public GameObject target;
    public Vector3 offsetVector;

    void Update(){
        // setze die camera auf Target-Position mit einem Offset (Abstand in 3 Dimensionen)
        transform.position = target.transform.position + offsetVector;
        // rotiere die camera so, dass sie immer auf die Position des targets schaut
        transform.LookAt(target.transform);
    }
}
